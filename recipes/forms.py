from recipes.models import Recipe
from django.forms import ModelForm
from django import forms

class RecipeForm(ModelForm):
    #add the meta subclass to hook into django's
    #premade form behavior
    class Meta:
        #Specify the model that we want
        model = Recipe
        fields = [
            "title",
            "picture",
            'description',
            'rating',
        ]

class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    first_name = forms.CharField(max_length=150)
    last_name = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
